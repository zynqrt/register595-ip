
`timescale 1 ns / 1 ps
    
    module ms(
        input wire [7:0] motorin,
        output wire [7:0] motorout
    );
        // ���������� ���� � ����������� � �������� � ������
        //                 nRESET[7]   nSLEEP[6]    DECAY[5]   DECAYZ[4]    nENBL[3]   MODE0[2]     MODE1[1]    MODE2[0]
        assign motorout = {motorin[0], motorin[2], motorin[6], motorin[7], motorin[1], motorin[3], motorin[4], motorin[5]};
    endmodule
    
    
	module Register595_v1_0 #
	(
		// Users to add parameters here
		parameter integer C_CONTROL_REG_WIDTH = 10,
		parameter integer C_MOTOR_LED_DATA_WIDTH = 8,
		parameter integer C_IRQ_WIDTH = 1,

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
		// Users to add ports here
		
		output wire MC_RESET,
		output wire MC_DATA,
		output wire MC_CLOCK,
		output wire MC_LATCH,
		output wire irq,

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);

	wire [C_CONTROL_REG_WIDTH - 1 : 0] reg_cr;
	wire [C_MOTOR_LED_DATA_WIDTH-1 : 0] reg_ms0;
	wire [C_MOTOR_LED_DATA_WIDTH-1 : 0] reg_ms1;
	wire [C_MOTOR_LED_DATA_WIDTH-1 : 0] reg_ms2;
	wire [C_MOTOR_LED_DATA_WIDTH-1 : 0] reg_ms3;
	wire [C_MOTOR_LED_DATA_WIDTH-1 : 0] reg_led;
	wire regs_updated;
	wire status_ready;
	wire status_error;

// Instantiation of Axi Bus Interface S00_AXI
	Register595_v1_0_S00_AXI # (
		.C_CONTROL_REG_WIDTH(C_CONTROL_REG_WIDTH),
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) Register595_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),

		.reg_cr       (reg_cr),
		.reg_ms0      (reg_ms0),
		.reg_ms1      (reg_ms1),
		.reg_ms2      (reg_ms2),
		.reg_ms3      (reg_ms3),
		.reg_led      (reg_led),
		.regs_updated (regs_updated),
		.status_ready (status_ready),
		.status_error (status_error)
	);

	// Add user logic here
	wire [7:0] led = {reg_led[0], reg_led[1], reg_led[2], reg_led[3], reg_led[4], reg_led[5], reg_led[6], reg_led[7]};
	wire [7:0] ms0;
	wire [7:0] ms1;
	wire [7:0] ms2;
	wire [7:0] ms3;
	
	ms ms_i0(.motorin(reg_ms0), .motorout(ms0));
	ms ms_i1(.motorin(reg_ms1), .motorout(ms1));
	ms ms_i2(.motorin(reg_ms2), .motorout(ms2));
	ms ms_i3(.motorin(reg_ms3), .motorout(ms3));
	
   	Chain595 #(
		.C_IRQ_WIDTH(C_IRQ_WIDTH),
		.C_REGISTER_DATA_SIZE (5*8),
		.C_REGISTER_CR_SIZE   (C_CONTROL_REG_WIDTH)
	) Chain595_inst (
		.aclk    (s00_axi_aclk),
		.aresetn (s00_axi_aresetn),
		.reg_cr  (reg_cr),
		.reg_data({led, ms3, ms2, ms1, ms0}),
		.regs_updated(regs_updated),
		.ready       (status_ready),
		.error       (status_error),
		
		.MC_RESET(MC_RESET),
		.MC_DATA (MC_DATA),
		.MC_CLOCK(MC_CLOCK),
		.MC_LATCH(MC_LATCH),
		.irq         (irq)
	);

	// User logic ends

	endmodule
