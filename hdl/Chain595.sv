module Chain595 #
	(
		parameter integer C_REGISTER_CR_SIZE = 9,
		parameter integer C_REGISTER_DATA_SIZE = 5*8,
		parameter integer C_IRQ_WIDTH = 2
	)
	(
		input wire aclk,
		input wire aresetn,

		input wire [C_REGISTER_CR_SIZE - 1 : 0] reg_cr,
		input wire [C_REGISTER_DATA_SIZE - 1 : 0] reg_data,
		input wire regs_updated,

		output reg ready,
		output wire error,
		output wire irq,

		output wire MC_RESET,
		output wire MC_DATA,
		output wire MC_CLOCK,
		output wire MC_LATCH
	);


assign MC_RESET = reg_cr[8];
wire irq_enable = reg_cr[9];

wire [3:0] data_phase;
assign data_phase = reg_cr[3:0];
wire [3:0] clk_devider;
assign clk_devider = reg_cr[7:4];


reg [C_REGISTER_DATA_SIZE - 1 : 0] shift_reg;
reg [5:0] bit_cnt;
assign MC_DATA = shift_reg[C_REGISTER_DATA_SIZE - 1];


assign error = (data_phase > clk_devider) | (clk_devider == 0) | (clk_devider == 1);
wire need_shift;

reg update_pending;
reg clk_rise;
reg clk_fall;
reg latch;

	enum bit [2:0] {
		IDLE = 'h0,
		LOAD,
		SHIFT,
		LATCH
	} state;

	always_ff @(posedge aclk) begin
		if (!aresetn) begin
			state <= IDLE;
			shift_reg <= 0;
			bit_cnt <= 0;
			update_pending <= 1'b0;
			latch <= 1'b0;
			ready <= 1'b0;
		end
		else begin
			
			if (regs_updated) begin
				update_pending <= 1'b1;
				state <= IDLE;
			end
			
			if (error) begin
				state <= IDLE;
			end

			case (state)
				IDLE: begin
					ready <= 1'b1;

					bit_cnt <= 0;
					latch <= 1'b0;
					if (update_pending) begin
						update_pending <= 1'b0;
						shift_reg <= reg_data;
						ready <= 1'b0;
						state <= LOAD;
					end
				end
				
				// Просто пустая задержка
				LOAD: begin
					state <= SHIFT;
				end

				SHIFT: begin
					if (need_shift) begin
						shift_reg <= {shift_reg[C_REGISTER_DATA_SIZE - 2 : 0], shift_reg[C_REGISTER_DATA_SIZE - 1]};
						bit_cnt <= bit_cnt + 1;
					end

					if (clk_fall & (bit_cnt == C_REGISTER_DATA_SIZE)) begin
						state <= LATCH;
						latch <= 1'b1;
					end
				end

				LATCH: begin
					if (clk_rise) begin
						latch <= 1'b0;
						state <= IDLE;
					end
				end
			endcase
		end
	end

assign MC_LATCH = latch;

	// Clock generator
reg [3:0] clk_cnt;
reg clk_reg;

	always_ff @(posedge aclk) begin
		if (!aresetn) begin
			clk_reg <= 1'b0;
			clk_cnt <= 0;
			clk_rise <= 0;
			clk_fall <= 0;
		end
		else begin

			clk_rise <= (clk_cnt == clk_devider) & (~clk_reg);
			clk_fall <= (clk_cnt == clk_devider) & clk_reg;

			if (state == SHIFT || state == LATCH) begin
				if (clk_cnt < clk_devider) begin
					clk_cnt <= clk_cnt + 1'b1;
				end
				else begin
					clk_cnt <= 0;
					clk_reg <= ~clk_reg;
				end
			end
			else begin
				clk_reg <= 1'b0;
				clk_cnt <= 0;
			end
		end
	end

assign need_shift = (clk_cnt == data_phase) & clk_reg;
assign MC_CLOCK = clk_reg & (state == SHIFT);


//***************************************************
// IRQ generator
reg irq_reg;
assign irq = irq_reg;

wire ready_rise;
	edge_detect ed_irq_rise(
		.clk(aclk),
		.in(ready),
		.rise(ready_rise));
	
	generate
		if (C_IRQ_WIDTH == 1) begin
			always_ff @(posedge aclk) begin
				irq_reg <= ready_rise & irq_enable;
			end
		end
		else begin
			reg [C_IRQ_WIDTH - 2 : 0] irq_dff;
			always_ff @(posedge aclk) begin
				if (!aresetn) begin
					irq_dff <= 0;
				end
				else begin
					irq_dff <= irq_dff << 1;
					irq_dff[0] <= ready_rise;
					irq_reg <= ((|irq_dff) | ready_rise) & irq_enable;
				end
			end
		end
	endgenerate
endmodule



/* Детектор фронтов */
module edge_detect(clk, in, rise, fall);
input clk;

input in;
output rise, fall;

reg dff;
	always @(posedge clk)
		dff <= in;
	
wire rise;
wire fall;
assign rise = in & (!dff);
assign fall = (!in) & dff;

endmodule

